//
//  CALayer+UIColor.m
//  Shmoozfest
//
//  Created by Nikita Kolpashikov on 6/22/15.
//  Copyright (c) 2015 Match Events. All rights reserved.
//

#import "CALayer+UIColor.h"

@implementation CALayer (UIColor)

- (UIColor *)borderUIColor {
    return [UIColor colorWithCGColor:self.borderColor];
}

- (void)setBorderUIColor:(UIColor *)borderUIColor {
    self.borderColor = borderUIColor.CGColor;
}

@end
