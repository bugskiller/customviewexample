//
//  CALayer+UIColor.h
//  Shmoozfest
//
//  Created by Nikita Kolpashikov on 6/22/15.
//  Copyright (c) 2015 Match Events. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

@interface CALayer (UIColor)

@property (nonatomic, assign) UIColor *borderUIColor;

@end
