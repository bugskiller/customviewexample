//
//  AlbumInfoViewController.m
//  CustomViewExample
//
//  Created by Oleg Saraseko on 9/27/15.
//  Copyright (c) 2015 No. All rights reserved.
//

#import "AlbumInfoViewController.h"

@interface AlbumInfoViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *artistLabel;
@property (weak, nonatomic) IBOutlet UILabel *yearLabel;
@property (weak, nonatomic) IBOutlet UILabel *genreLabel;
@property (weak, nonatomic) IBOutlet UILabel *ratingLabel;
@property (weak, nonatomic) IBOutlet UIView *detailsContainerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *detailsContainerViewBottomConstraint;

@end

@implementation AlbumInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self hideDetailsContainerViewAnimated:NO];
    
    [self setupUI];
}

- (void)setAlbum:(Album *)album {
    
    _album = album;
    
    [self setupUI];
}

- (void)setupUI {
    
    if (_album) {
        
        self.titleLabel.text = self.album.title;
        self.artistLabel.text = self.album.artist;
        self.yearLabel.text = [NSString stringWithFormat:@"%lu", self.album.year];
        self.genreLabel.text = self.album.genre;
        self.ratingLabel.text = [NSString stringWithFormat:@"%lu/10", self.album.rating];
        
        if (self.album.image) {
            
            self.imageView.image = self.album.image;
        }
    }
}

- (IBAction)moreTapped:(id)sender {
    
    [self toggleDetailsView];
}

- (void)toggleDetailsView {
    
    if (self.detailsContainerViewBottomConstraint.constant >= 0) {
        
        [self hideDetailsContainerViewAnimated:YES];
    }
    else {
        
        [self showDetailsContainerViewAnimated:YES];
    }
}

- (void)hideDetailsContainerViewAnimated:(BOOL)animated {
    
    self.detailsContainerViewBottomConstraint.constant = -CGRectGetHeight(self.detailsContainerView.frame);
    
    if ([self.delegate respondsToSelector:@selector(resizeViewController:toSize:animated:)]) {
        
        [self.delegate resizeViewController:self toSize:CGSizeMake(CGRectGetWidth(self.view.frame), 199 - CGRectGetHeight(self.detailsContainerView.frame)) animated:animated];
    }
}

- (void)showDetailsContainerViewAnimated:(BOOL)animated {
    
    self.detailsContainerViewBottomConstraint.constant = 0;
    
    if ([self.delegate respondsToSelector:@selector(resizeViewController:toSize:animated:)]) {
        
        [self.delegate resizeViewController:self toSize:CGSizeMake(CGRectGetWidth(self.view.frame), 199) animated:animated];
    }
}

@end
