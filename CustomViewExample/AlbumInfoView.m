//
//  AlbumInfoView.m
//  CustomViewExample
//
//  Created by Oleg Saraseko on 9/28/15.
//  Copyright (c) 2015 No. All rights reserved.
//

#import "AlbumInfoView.h"

@interface AlbumInfoView ()
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *artistLabel;
@property (weak, nonatomic) IBOutlet UILabel *yearLabel;
@property (weak, nonatomic) IBOutlet UILabel *genreLabel;
@property (weak, nonatomic) IBOutlet UILabel *ratingLabel;
@property (weak, nonatomic) IBOutlet UIView *detailsContainerView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomContainerViewConstraint;

@end

@implementation AlbumInfoView

+ (instancetype)viewWithXib {
    
    NSBundle *bundle = [NSBundle bundleForClass:self];
    return [[bundle loadNibNamed:NSStringFromClass(self) owner:nil options:nil] firstObject];
}

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [self.class viewWithXib];
    if (self) {
        
        self.frame = frame;
    }
    
    return self;
}

- (id)awakeAfterUsingCoder:(NSCoder *)aDecoder
{
    if (![self.subviews count])
    {
        UIView *loadedView = [self.class viewWithXib];
        loadedView.frame = self.frame;
        loadedView.autoresizingMask = self.autoresizingMask;
        loadedView.translatesAutoresizingMaskIntoConstraints = self.translatesAutoresizingMaskIntoConstraints;
        
        return loadedView;
    }
    return self;
}

- (void)setAlbum:(Album *)album {
    
    _album = album;
    
    if (_album) {
        
        self.titleLabel.text = self.album.title;
        self.artistLabel.text = self.album.artist;
        self.yearLabel.text = [NSString stringWithFormat:@"%lu", self.album.year];
        self.genreLabel.text = self.album.genre;
        self.ratingLabel.text = [NSString stringWithFormat:@"%lu/10", self.album.rating];
        
        if (self.album.image) {
            
            self.imageView.image = self.album.image;
        }
    }
}

- (IBAction)moreTapped:(id)sender {
    
    [self toggleDetailsView];
}

- (void)toggleDetailsView {
    
    if (self.detailsContainerView.hidden) {
        
        [self showDetailsContainerViewAnimated:YES];
    }
    else {
        
        [self hideDetailsContainerViewAnimated:YES];
    }
}

- (void)hideDetailsContainerViewAnimated:(BOOL)animated {
    
    self.bottomContainerViewConstraint.constant = -CGRectGetHeight(self.detailsContainerView.frame);
    
    if (animated) {
        
        if ([self.delegate respondsToSelector:@selector(animateViewWithDuration:delay:options:completion:)]) {
            
            [self.delegate animateViewWithDuration:0.3
                                             delay:0.0
                                           options:UIViewAnimationOptionCurveEaseIn
                                        completion:^(BOOL finished) {
                                            
                                            self.detailsContainerView.hidden = YES;
                                        }];
        }
    }
    else {
        
        self.detailsContainerView.hidden = YES;
        [self layoutIfNeeded];
    }
}

- (void)showDetailsContainerViewAnimated:(BOOL)animated {
    
    self.detailsContainerView.hidden = NO;
    self.bottomContainerViewConstraint.constant = 0;
    
    if (animated) {
        
        if ([self.delegate respondsToSelector:@selector(animateViewWithDuration:delay:options:completion:)]) {
            
            [self.delegate animateViewWithDuration:0.3
                                             delay:0.0
                                           options:UIViewAnimationOptionCurveEaseIn
                                        completion:nil];
        }
    }
    else {
        
        [self layoutIfNeeded];
    }
}

@end
