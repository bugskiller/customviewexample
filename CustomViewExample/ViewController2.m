//
//  ViewController2.m
//  CustomViewExample
//
//  Created by Oleg Saraseko on 9/28/15.
//  Copyright (c) 2015 No. All rights reserved.
//

#import "ViewController2.h"
#import "AlbumInfoView.h"

@interface ViewController2 () <ViewControllerAnimationProtocol>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet AlbumInfoView *albumInfoView;

@property (nonatomic) NSArray *songs;

@end

@implementation ViewController2

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.albumInfoView.album = self.album;
    self.albumInfoView.delegate = self;
    
    [self.albumInfoView hideDetailsContainerViewAnimated:NO];
    
    int numSongs = 10;
    NSMutableArray *songsArray = [NSMutableArray arrayWithCapacity:numSongs];
    for (int i = 0; i < numSongs; i++) {
        
        [songsArray addObject:
         [NSString stringWithFormat:@"Song #%d", i]];
    }
    self.songs = [NSArray arrayWithArray:songsArray];
}

#pragma mark - <ViewControllerAnimationProtocol>

- (void)animateViewWithDuration:(NSTimeInterval)duration
                          delay:(NSTimeInterval)delay
                        options:(UIViewAnimationOptions)options
                     completion:(void (^)(BOOL finished))completion
{
    
    [UIView animateWithDuration:duration
                          delay:delay
                        options:options
                     animations:^{
                         
                         [self.view layoutIfNeeded];
                     }
                     completion:^(BOOL finished) {
                         
                         if (completion) {
                             
                             completion(finished);
                         }
                     }];

}

#pragma mark - <UITableViewDatasource>

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.songs.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *const kCellIdentifier = @"VCCellIdentifier";
    UITableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:kCellIdentifier];
    if (!cell) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kCellIdentifier];
    }
    
    cell.textLabel.text = self.songs[indexPath.row];
    
    return cell;
}

@end
