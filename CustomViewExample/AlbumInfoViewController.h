//
//  AlbumInfoViewController.h
//  CustomViewExample
//
//  Created by Oleg Saraseko on 9/27/15.
//  Copyright (c) 2015 No. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlbumInfoView.h"


@protocol ViewControllerResizable <NSObject>
@optional
- (void)resizeViewController:(UIViewController *)vc toSize:(CGSize)size animated:(BOOL)animated;

@end

@interface AlbumInfoViewController : UIViewController
@property (nonatomic) Album *album;
@property (nonatomic, weak) id<ViewControllerResizable> delegate;

@end
