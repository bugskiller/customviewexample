//
//  Album.h
//  CustomViewExample
//
//  Created by Oleg Saraseko on 9/27/15.
//  Copyright (c) 2015 No. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface Album : NSObject
@property (nonatomic) NSString *title;
@property (nonatomic) NSString *artist;
@property (nonatomic) NSUInteger year;
@property (nonatomic) NSString *genre;
@property (nonatomic) NSUInteger rating;
@property (nonatomic) UIImage *image;

@property (nonatomic) NSArray *songs;

@end
