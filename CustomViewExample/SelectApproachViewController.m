//
//  SelectApproachViewController.m
//  CustomViewExample
//
//  Created by Oleg Saraseko on 9/28/15.
//  Copyright (c) 2015 No. All rights reserved.
//

#import "SelectApproachViewController.h"
#import "ViewController.h"
#import "ViewController2.h"

@interface SelectApproachViewController ()
@property (nonatomic) Album *album;

@end

@implementation SelectApproachViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.album = [Album new];
    self.album.title = @"Slice Of Life";
    self.album.artist = @"Daniel Wanrooy";
    self.album.year = 2011;
    self.album.genre = @"Trance";
    self.album.rating = 9;
    self.album.image = [UIImage imageNamed:@"ic_wanrooy"];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"ContainmentFeature"]) {
        
        ((ViewController *)segue.destinationViewController).album = self.album;
    }
    else if ([segue.identifier isEqualToString:@"ViewFromXIB"]) {
        
        ((ViewController2 *)segue.destinationViewController).album = self.album;
    }
}

@end
