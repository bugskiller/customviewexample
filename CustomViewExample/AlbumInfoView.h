//
//  AlbumInfoView.h
//  CustomViewExample
//
//  Created by Oleg Saraseko on 9/28/15.
//  Copyright (c) 2015 No. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ViewControllerAnimationProtocol <NSObject>
@optional
- (void)animateViewWithDuration:(NSTimeInterval)duration
                          delay:(NSTimeInterval)delay
                        options:(UIViewAnimationOptions)options
                     completion:(void (^)(BOOL finished))completion;

@end

IB_DESIGNABLE
@interface AlbumInfoView : UIView

@property (nonatomic) Album *album;
@property (nonatomic, weak) id<ViewControllerAnimationProtocol> delegate;

- (void)hideDetailsContainerViewAnimated:(BOOL)animated;
- (void)showDetailsContainerViewAnimated:(BOOL)animated;

@end
