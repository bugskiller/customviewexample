//
//  ViewController.m
//  CustomViewExample
//
//  Created by Oleg Saraseko on 9/25/15.
//  Copyright (c) 2015 No. All rights reserved.
//

#import "ViewController.h"
#import "AlbumInfoViewController.h"

@interface ViewController () <ViewControllerResizable>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *containerViewHeightConstraint;

@property (nonatomic) NSArray *songs;

@end

@implementation ViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    int numSongs = 10;
    NSMutableArray *songsArray = [NSMutableArray arrayWithCapacity:numSongs];
    for (int i = 0; i < numSongs; i++) {
        
        [songsArray addObject:
         [NSString stringWithFormat:@"Song #%d", i]];
    }
    self.songs = [NSArray arrayWithArray:songsArray];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"Embed"]) {
        
        ((AlbumInfoViewController *)segue.destinationViewController).album = self.album;
        ((AlbumInfoViewController *)segue.destinationViewController).delegate = self;
    }
}


#pragma mark - <ViewControllerResizable>

- (void)resizeViewController:(UIViewController *)vc toSize:(CGSize)size animated:(BOOL)animated {
    
    self.containerViewHeightConstraint.constant = size.height;
    
    if (animated) {
        
        [UIView animateWithDuration:0.3
                              delay:0
                            options:UIViewAnimationOptionCurveEaseIn
                         animations:^{
                             
                             [self.view layoutIfNeeded];
                         }
                         completion:nil];
    }
    else {
        
        [self.view layoutIfNeeded];
    }
}

#pragma mark - <UITableViewDatasource>

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.songs.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *const kCellIdentifier = @"VCCellIdentifier";
    UITableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:kCellIdentifier];
    if (!cell) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kCellIdentifier];
    }
    
    cell.textLabel.text = self.songs[indexPath.row];
    
    return cell;
}

@end
