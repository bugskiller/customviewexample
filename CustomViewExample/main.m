//
//  main.m
//  CustomViewExample
//
//  Created by Oleg Saraseko on 9/25/15.
//  Copyright (c) 2015 No. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
