//
//  AppDelegate.h
//  CustomViewExample
//
//  Created by Oleg Saraseko on 9/25/15.
//  Copyright (c) 2015 No. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

